<?php
  include '../views/header.php';

?>

<nav class="navbar navbar-expand-lg  navbar-dark bg-dark" style="margin-bottom: 80px">
  	<a class="navbar-brand" href="#">Questionnaires.Net</a>
  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
  	</button>
  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
   	    <ul class="navbar-nav mr-auto">
     	  <li class="nav-item active">
      	   <a class="nav-link" href="/index.php">Home</a>
      	  </li>
    	</ul>
    	<form class="form-inline my-2 my-lg-0">
      		<a class="btn btn-primary my-2 my-sm-0" href="/action/login.php" role="button">Login</a>
    	</form>
	</div>
</nav>

<div class="container">
  <h1 class="text-center text-light">Welcome to Questionaries.Net</h1>
  <div class="row justify-content-md-center">
    <div class="col-md-4 md-auto d-block">
      <div class="card text-white bg-dark mb-3">
      <!--funcion del login, que lo direcciona al archivo de validation.php-->
        <h2 class="card-header text-center" >Login</h2>
        <div class="card-body">
          <form action="/action/validation.php" method="POST">
            <div class="form-group">
              <label>Username</label>
              <input type="text" name="username" id="" class="form-control" placeholder="username">
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" name="passwd" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Login</button>
        </form> 
        </div> 
      </div> 
    </div>

    <!--*******************************************************************************************************-->

    <div class="col-md-4 md-auto d-block">
      <div class="card text-white bg-dark mb-3">
      <!--funcion del registro, que lo direcciona al archivo del registe.php-->
        <h2 class="card-header text-center ">Register</h2>
        <div class="card-body">
          <form action="/action/register.php" method="POST">
            <div class="form-group">
              <label>Username</label>
              <input type="text" name="username" id="" class="form-control" placeholder="username">
            </div>
           <div class="form-group">
              <label>Password</label>
              <input type="password" name="passwd" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Sing in</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include '../views/footer.php'; ?>