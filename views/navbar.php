<nav class="navbar navbar-expand-lg  navbar-dark bg-dark" style="margin-bottom: 80px">
	<a class="navbar-brand" href="#">Questionaires.Net</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
   	    <ul class="navbar-nav mr-auto">
     	  	<li class="nav-item active">
      	   		<a class="nav-link" href="/Questionaires/home.php">Apply Questionnaires</a>
      	  	</li>
      	  	<li class="nav-item">
      	    	<a class="nav-link" href="#">Results Questinnaires</a>
      	  	</li>
    	</ul>
    	<form class="form-inline my-2 my-lg-0">
    		<a class="btn btn-primary my-2 my-sm-0" href="/action/logout.php" role="button">LOGOUT</a>
		</form>
	</div>
</nav>