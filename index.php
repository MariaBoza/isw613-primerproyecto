<?php 
    include './views/header.php';
?>

<nav class="navbar navbar-expand-lg  navbar-dark bg-dark" style="margin-bottom: 80px">
  	<a class="navbar-brand" href="#">Questionnaires.Net</a>
  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
  	</button>
  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
   	    <ul class="navbar-nav mr-auto">
     	  <li class="nav-item active">
      	   <a class="nav-link" href="/index.php">Home</a>
      	  </li>
    	</ul>
    	<form class="form-inline my-2 my-lg-0">
      		<a class="btn btn-primary my-2 my-sm-0" href="/action/login.php" role="button">Login</a>
    	</form>
	</div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-lg-10 ">
            <div class="jumbotron">
                <h1 class="display-4">Welcome, Questionnaires.Net</h1>
                <p class="lead">We will apply different questionnaires to evaluate your knowledge.</p>
                <hr class="my-4">
                <p>You want to start, challenge yourself!.</p>
               <a class="btn btn-dark btn-lg" href="/action/login.php" role="button">Login</a>
            </div>
        </div>
    </div>
</div>


<?php include './views/footer.php'; ?>
