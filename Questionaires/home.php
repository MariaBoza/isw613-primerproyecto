<?php
    include '../views/header.php';
    include '../views/navbar.php';
        
    session_start(); 
    if(!isset($_SESSION['username'])){
        header('location:/action/login.php');
    }   
    $con = mysqli_connect('localhost','isw613_user','secret','isw613_questionnaires','3306');
    mysqli_select_db($con, 'isw613_questionnaires');     
?>
    <div class="container">
        <h2 class="text-right text-light">Welcome : <label class="text-primary"><?php echo $_SESSION['username'];?></label></h2>
        <h1 class="text-center text-light">Select the questionnaire you wish to complete!</h1>
        <div class="row justify-content-md-center">
            
            <div class="col-md-9">
                <div class="card border-primary" style="max-width: 42rem;">
                    <form action="/Questionaires/logic/homeQuestionnaires.php" method="POST">

                        <div class="card-header text-center text-primary">Questionnaires</div>
                            <!-- Selector para los cuestionarios -->
                        <div class="card-body text-dark">
                            <select class="custom-select" name="description">
                                <option selected>Choose...</option>
                            <?php 
                                $sql = "select * from questionnaires";
                                $result = mysqli_query($con, $sql) or die(mysqli_error($con));
                            ?>
                            <?php while($row = mysqli_fetch_array($result)): ?>
                                <option value="<?php echo $row['id']?>" ><?php echo $row['description']?>, <label for=""><?php echo $row['long_description']?></label></option>
                            <?php endwhile ?>
                            </select>
                            <div class="card-body mx-auto">
                                <input class="btn btn-primary btn-lg" type="submit" value="Submit"> 
                            </div>
                                                                         
                        </div> 

                    </form>
                </div>
            </div>

               
        </div>
    </div>
<?php include '../views/footer.php'; ?>