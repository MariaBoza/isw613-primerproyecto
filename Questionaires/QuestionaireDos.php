<?php
     include '../views/header.php';
     include '../views/navbar.php';
     session_start();
     if(!isset($_SESSION['username'])){
          header('location:login.php');
     }
     $con = mysqli_connect('localhost','isw613_user','secret','isw613_questionnaires','3306');
     mysqli_select_db($con, 'isw613_questionnaires');
?>

     <div class="container">
          <h2 class="text-right text-light">Welcome : <label class="text-primary"><?php echo $_SESSION['username'];?></label></h2>

        <div class="col-lg-12 md-auto d-block">
            <div class="card border-primary" >
			<?php 
				$sql3 = "select * from questionnaires where id = 2";
				 $query3 = mysqli_query($con, $sql3);
				 while ($row3 = mysqli_fetch_array($query3)) {
			?>	
				<h4 class="text-center card-header"><?php echo $row3['long_description'] ?></h4>
			<?php
				}
			?>
			</div>
			<form action="#" method="POST">
				<!-- seleciona las preguntas, donde se encuentran en la tabla questions -->
				<?php 
					for ($i=6; $i <11 ; $i++) { 
						$sql = "select * from questions where id = $i";
						$query = mysqli_query($con, $sql);
						while ($row = mysqli_fetch_array($query)) {
				?>
					<!-- seleciona las respuestas, donde se encuentran en la tabla answers -->
					<div class="card border-primary" >
						<h4 class="card-header"><?php echo $row['question_text'] ?></h4>
						<?php
							$sql2 = "select * from answers where number = $i";
							$query2 = mysqli_query($con, $sql2);
							while ($row2 = mysqli_fetch_array($query2)) {
						?>
						<div class="card-body text-dark">
							<input type="radio" name="quizcheck[<?php echo $row2['number']; ?>]" value="<?php echo $row2['answer_points']; ?>">
							<label for=""><?php echo $row2['answer_text'];?></label>
						</div>
					
					<?php
						}
						}
					}
					?>
					</div>
				<div class="card-body mx-auto">
					<input type="submit" name="submit" value="submit" class="btn btn-primary btn-lg">
					<a href="/Questionaires/home.php" class="btn btn-primary btn-lg">Cancel</a>
				</div>
			</form>
		</div>
     </div>



<?php include '../views/footer.php';  ?>